# Front-end dev Technical Test - Dishwasher App

## Brief

Your task is to add an Add to Basket feature to the John Lewis Dishwasher App. 

There are two page templates in the application, the Product Grid Page and Product Details Page. 

### Product Details Page

On the Product Details Page, add a button labelled "Add to Basket". When clicked, this should add the current item to the basket with the following details:

```
Product title
Product ID
Price
Quantity
```

If you click the button a second time, it should increase the quantity in the basket by 1. 

### Product Grid Page

Add a button on the Product Grid Page which, when clicked, will show the basket contents with the following details:

```
Items
Quantities
Total price
```

## Things we're looking for

- The use of third party code/SDKs is allowed, but you should be able to explain why you have chosen the third party code.
- We’d like to see a TDD approach to writing the app, we've included a Jest setup for unit tests.
- Put all your assumptions, notes and instructions into your GitHub README.md.
- We'll be assessing your coding style, how you've approached this task and whether you've met quality standards.
- You shouldn't spend more than 3 hours on this task.
---

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account)
- You will need Node.js installed
- Open the terminal go to the project folder with `cd jlp-pl8-test-front-end`
- Install the dependencies using `npm i`
- Run the development server with `nmp run dev`
- Open [http://localhost:3000](http://localhost:3000) with your browser.
