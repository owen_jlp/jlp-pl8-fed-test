import styles from "./product-specification.module.scss";

const ProductSpecification = ({ specifications }) => {
  console.log(specifications);
  return (
    <div className={styles.productSpecification}>
      <h3>Product specification</h3>
      <ul className={styles.specificationItems}>
        {specifications.map((item) => (
          <li>
            <div>{item.name}</div>
            <div>{item.value}</div>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default ProductSpecification;
